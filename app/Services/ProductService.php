<?php
namespace App\Services;

use App\Repositeries\ProductRepositery;

class ProductService{
    /**
     *@var $productRepositery
     */

     protected $productRepositery;

     /**
      * productService Constructor
      *@param ProductRepositery $productRepositery
      */

      public function __construct(ProductRepositery $productRepositery){
        $this->productRepositery = $productRepositery;
      }

       /**
       * get products in db
       */
      public function index(){
        return $this->productRepositery->index();
      }

       /**
       * store product in db
       */
      public function store($data){
        return $this->productRepositery->store($data);//store products
      }

       /**
       * update product in db
       */
      public function edit($data, $id){
        try{
            return $this->productRepositery->edit($data, $id);//update
        }catch(\Exception $exception){
            return response()->json(['message' => $exception->getMessage()]);
        }
      }

      /**
      * delete product from db
      */
      public function destroy($id){
        try{
            return $this->productRepositery->destroy($id);
        }catch(\Exception $exception){
            return response()->json(['message' => $exception->getMessage()]);
        }
      }

      /**
      * get categories from db
      */
      public function categories(){
        try{
            return $this->productRepositery->categories();
        }catch(\Exception $exception){
            return response()->json(['message' => $exception->getMessage()]);
        }
      }
}
