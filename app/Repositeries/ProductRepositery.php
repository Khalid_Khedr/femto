<?php
namespace App\Repositeries;

use App\Models\{Product,Category};
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductRepositery{
    /**
     *@var Product
     */

     protected $product;

     /**
      * ProductRepositery Constructor
      *@param Product $product
      */

      public function __construct(Product $product){
        $this->product = $product;
      }

      /**
       * get all products with user and categories
       */
      public function index(){
        return $this->product::with(['user', 'category'])->get();
      }

       /**
       * store product in db
       */
      public function store($data){
        return DB::transaction(function () use ($data) {
        $readyData = collect($data)->except('categories')->toArray(); //except categories
        $readyData['user_id'] = Auth::id();// add user who add product
        $product = $this->product->create($readyData); //craete product without categories
        $product->category()->attach($data['categories']); // craete many to many category
        return true;
        });
      }

       /**
       * update product in db
       */
      public function edit($data, $id){
        $product = $this->product::find($id);//find product with id
        return $product->update($data);//update product only
      }

      /**
       * delete product from db
       */
      public function destroy($id){
        return $this->product->find($id)->delete();// find product and delete it with his categories
      }

       /**
       * get categories from db
       */
      public function categories(){
        return Category::all(); // get all categories
      }
}

