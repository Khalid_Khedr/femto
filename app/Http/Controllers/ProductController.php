<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProductService;
use DataTables;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    protected $service;

    public function __construct(ProductService $service){
        $this->service = $service;
    }

    public function index(){
        $products = $this->service->index();
        return Datatables::of($products)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm">Edit</a> <a href="javascript:void(0)" data-rowid="'.$row->id.'" class="delete btn btn-danger btn-sm">Delete</a>';
                    return $actionBtn;
                })
                ->addColumn('category', function($categoies){
                    $category_text = '';
                    foreach($categoies->category as $category)
                    {
                        $category_text .= $category->type.' - ';
                    }
                    return $category_text;
                })
                ->rawColumns(['action', 'category'])
                ->make(true);
    }

    public function categories(){
        return $this->service->categories();
    }

    public function store(ProductRequest $request){
        return $this->service->store($request->validated());
    }

    public function update(ProductRequest $request, $id){
        return $this->service->edit($request->validated(), $id);
    }

    public function destroy($id){
        $this->service->destroy($id);
        return ['success' => true, "message" => "deleted"];
    }
}
