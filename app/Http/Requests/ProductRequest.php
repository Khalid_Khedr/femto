<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return request()->method() === 'POST' ? $this->storeRules() : $this->updateRules();
    }

    private function storeRules(){
        return [
            'name' => "required|string",
            'price' => "required|numeric",
            'quantity' => "required|numeric",
            'categories' => 'required|array',
        ];
    }

    private function updateRules(){
        return [
            'name' => "required|string",
            'price' => "required|numeric",
            'quantity' => "required|numeric",
        ];
    }
}
