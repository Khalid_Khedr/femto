@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <button type="button" class="btn btn-success product_add">Add Product</button>
                    <div class="container mt-5">
                        <h2 class="mb-4">Products</h2>
                        <table class="table table-bordered yajra-datatable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>name</th>
                                    <th>quantity</th>
                                    <th>price</th>
                                    <th>category</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                        <div class="modal"  role="dialog" style="z-index: 1051 !important;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Product</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                <form class="product_form">
                                    <input type="hidden" name="id">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Product Name</label>
                                        <input type="text" name="name" class="form-control" id="product_name" aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Product Price</label>
                                        <input type="number" name="price" class="form-control" id="product_price" aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Product Quantity</label>
                                        <input type="number" name="quantity" class="form-control" id="product_price" aria-describedby="emailHelp" placeholder="Enter email">
                                    </div>
                                    <div class="form-group select_box">
                                        <label for="Category">Category</label>
                                        <select name="categories[]" id="mySelect2" class="form-control" multiple="multiple" >
                                        </select>
                                    </div>
                                </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary save">Save changes</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    /**
     * get all products and paginate it
     */
    var table = $('.yajra-datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('product.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'quantity', name: 'quantity'},
            {data: 'price', name: 'price'},
            {data: 'category', name: 'category'},
            {
                data: 'action',
                name: 'action',
                orderable: true,
                searchable: true
            },
        ]
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

     /**
     * setup select2 library
     */
    $('#mySelect2').select2({
        selectOnClose: true,
        placeholder: "Categories",
    });

    /**
     * get all categories
     */
    function Categories(){
        $.ajax({
            url: "/categories",
            type: 'get',
            dataType: "JSON",
            success: function(data){
                $.each(data, function (i, item) {
                $('#mySelect2').append($('<option>', {
                    value: item.id,
                    text : item.type
                }));
            });
            }
        });
    }

    /**
     * open modal and insert product to create then save
     */
    $(document).on('click', '.product_add', function(){
        $('.modal').find('.modal-title').text('Create Product');
        $('.select_box').css('display', 'block');
        $('.modal').modal('show');
        Categories();
        $('.save').click(function(){
            $validate_status = 0;
            var data = $('.product_form').serializeArray();
            $.each(data, function (index,value) {
                if(value.name != 'id' && value.value == '' || value.value == null){
                    alert("all fields required");
                    $validate_status = 1;
                    return false;
                }
            });

            if($validate_status == 1){
                $validate_status == 0;
                return;
            }
            console.log(data);
            $.ajax({
            url: "/product",
            type: 'POST',
            data: data,
            dataType: "JSON",
            success: function(data){
                table.draw();
                $('.modal').modal('hide');
            }
        });
        });

    });

    /**
     * open modal and edit product to update then save
     */
    $(document).on('click', '.edit', function(){
        $('.modal').find('.modal-title').text('Update Product');
        $('.select_box').css('display', 'none');
        $('.modal').modal('show');
        var rowData = table.row($(this).parents('tr')).data();
        $('.product_form').find('input[name="id"]').val(rowData.id);
        $('.product_form').find('input[name="name"]').val(rowData.name);
        $('.product_form').find('input[name="price"]').val(rowData.price);
        $('.product_form').find('input[name="quantity"]').val(rowData.quantity);
        $('.save').click(function(){
            $validate_status = 0;
            var data = $('.product_form').serializeArray();
            $.each(data, function (index,value) {
                if(value.value == '' || value.value == null){
                    alert("all fields required");
                    $validate_status = 1;
                    return false;
                }
            });
            if($validate_status == 1){
                $validate_status == 0;
                return;
            }
            console.log(data);
            $.ajax({
            url: "/product/"+rowData.id,
            type: 'PUT',
            data: data,
            dataType: "JSON",
            success: function(data){
                table.draw();
                $('.product_form').find('input[name="id"]').val('');
                $('.product_form').find('input[name="name"]').val('');
                $('.product_form').find('input[name="price"]').val('');
                $('.product_form').find('input[name="quantity"]').val('');
                $('.modal').modal('hide');
            }
         });
        });

    });

    /**
     * get product to delete then if choose yes then delete product
     */
    $(document).on('click', '.delete', function(){
        if(!confirm("Are you sure?")) return;
        var id = $(this).data('rowid');
        var el = $(this);
        console.log(id);
        $.ajax({
            url: "/product/"+id,
            type: 'DELETE',
            dataType: "JSON",
            success: function(data){
            if(data.success){
                table.row(el.parents('tr')).remove().draw();
            }
            }
        });
    });

});

</script>

@endsection

